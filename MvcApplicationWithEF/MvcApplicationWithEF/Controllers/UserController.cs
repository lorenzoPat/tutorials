﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplicationWithEF.Controllers
{
    public class UserController : Controller
    {
        private distributedsystemsEntities db = new distributedsystemsEntities();

        //
        // GET: /User/

        public ActionResult Index()
        {
            return View(db.users_asp.ToList());
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(long id = 0)
        {
            users_asp users_asp = db.users_asp.Find(id);
            if (users_asp == null)
            {
                return HttpNotFound();
            }
            return View(users_asp);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(users_asp users_asp)
        {
            if (ModelState.IsValid)
            {
                db.users_asp.Add(users_asp);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(users_asp);
        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(long id = 0)
        {
            users_asp users_asp = db.users_asp.Find(id);
            if (users_asp == null)
            {
                return HttpNotFound();
            }
            return View(users_asp);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(users_asp users_asp)
        {
            if (ModelState.IsValid)
            {
                db.Entry(users_asp).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(users_asp);
        }

        //
        // GET: /User/Delete/5

        public ActionResult Delete(long id = 0)
        {
            users_asp users_asp = db.users_asp.Find(id);
            if (users_asp == null)
            {
                return HttpNotFound();
            }
            return View(users_asp);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            users_asp users_asp = db.users_asp.Find(id);
            db.users_asp.Remove(users_asp);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}