package lorenzo.pat.RMIclient;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import lorenzo.pat.RMIinterface.Constant;
import lorenzo.pat.RMIinterface.RMIInterface;

public class ClientMain {

	public static void main(String[] args) throws RemoteException, NotBoundException {
		Registry registry = LocateRegistry.getRegistry("localhost", Constant.RMI_PORT);//change with ip address and it should work on remote comp
		RMIInterface remoteInterface = (RMIInterface) registry.lookup(Constant.RMI_ID);
		System.out.println(remoteInterface.isLoginValid("test"));
		System.out.println(remoteInterface.isLoginValid("test1"));
	}

}
