import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.logic.AdditionRemote;


public class Main {
	
	public static void main(String[] args) throws NamingException {
		
		InitialContext ic = new InitialContext();
		AdditionRemote ar = (AdditionRemote)ic.lookup("java:global/AdditionEJB/Addition!com.logic.AdditionRemote");
		System.out.println(ar.add(4, 5));
	}
}
