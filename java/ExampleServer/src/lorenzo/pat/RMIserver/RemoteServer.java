package lorenzo.pat.RMIserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import lorenzo.pat.RMIinterface.RMIInterface;

public class RemoteServer extends UnicastRemoteObject implements RMIInterface{

	protected RemoteServer() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;

	@Override
	public boolean isLoginValid(String username) throws RemoteException {
		if (username.equals("test"))
			return true;
		
		return false;
	}

}
