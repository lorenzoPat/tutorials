package lorenzo.pat.RMIserver;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import lorenzo.pat.RMIinterface.Constant;

public class ServerMain {
	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		RemoteServer remoteServer = new RemoteServer();
		Registry registry = LocateRegistry.createRegistry(Constant.RMI_PORT);
		registry.bind(Constant.RMI_ID, remoteServer);
		System.out.println("Server is started");
	}
}
