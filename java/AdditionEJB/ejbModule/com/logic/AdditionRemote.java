package com.logic;

import javax.ejb.Remote;

@Remote
public interface AdditionRemote {
	public int add(int x, int y);
}
