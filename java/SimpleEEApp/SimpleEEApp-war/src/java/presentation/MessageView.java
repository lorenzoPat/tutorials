/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package presentation;

import boundary.MessageFacadeLocal;
import entities.Message;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Lorenzo
 */
@ManagedBean(name = "messageView")
@RequestScoped
public class MessageView {
    @EJB
    private MessageFacadeLocal messageFacade;

    private Message message;
    
    public MessageView() {
        message = new Message();
    }
    
    public Message getMessage() {
       return message;
    }

    public int getNumberOfMessages(){
       return messageFacade.findAll().size();
    }

    // Saves the message and then returns the string "theend"
    public String postMessage(){
       this.messageFacade.create(message);
       return "theend";
    }
    
}
