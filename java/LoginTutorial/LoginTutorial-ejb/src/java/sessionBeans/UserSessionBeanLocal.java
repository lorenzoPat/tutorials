/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sessionBeans;

import entities.User;
import javax.ejb.Local;

/**
 *
 * @author Lorenzo
 */
@Local
public interface UserSessionBeanLocal {
    public void addUser(User u);
}
