/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sessionBeans;

import entities.User;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Lorenzo
 */
@Stateless
public class UserSessionBean implements UserSessionBeanLocal {
    @PersistenceContext(unitName = "LoginTutorial-ejbPU")
    EntityManager em;

    @Override
    public void addUser(User u) {
        em.persist(u);
    }

}
