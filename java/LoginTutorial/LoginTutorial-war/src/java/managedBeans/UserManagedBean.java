/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;

import entities.User;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import sessionBeans.UserSessionBeanLocal;

/**
 *
 * @author Lorenzo
 */
@ManagedBean(name="userManagedBean")
@SessionScoped
public class UserManagedBean {
    @EJB
    private UserSessionBeanLocal userSessionBean;
    private String username;
    private String password;
    private String message;
   
    public UserManagedBean() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getCompleted(){
        message = password;
        User u = new User();
        u.setId(212);
        u.setUsername(username);
        u.setPassword(password);
        u.setRole("Admin");
        userSessionBean.addUser(u);
        return "user";
    }
}
