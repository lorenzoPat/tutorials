package lorenzo.pat.RMIinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {
	public boolean isLoginValid(String username) throws RemoteException;
}
