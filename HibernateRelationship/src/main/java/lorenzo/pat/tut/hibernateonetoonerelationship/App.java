package lorenzo.pat.tut.hibernateonetoonerelationship;

import entities.Project;
import entities.Class;
import entities.Student;
import entities.StudentInformation;

import java.util.Date;
import org.hibernate.Session;

/**
 * Hello world!
 *
 */
public class App {

  public static void main(String[] args) {
    System.out.println("Hello World!");
    Session session = HibernateUtil.getSessionFactory().openSession();

    session.beginTransaction();

    Student student1 = new Student("Jeremny", "21");
    Student student2 = new Student("Robert", "21");

    Class class1 = new Class("Security", "Spring");
    Class class2 = new Class("Databases", "Winter");
    Class class3 = new Class("Hibernate", "Winter");

    student1.getStudentClasses().add(class1);
    student1.getStudentClasses().add(class2);

    student2.getStudentClasses().add(class1);
    student2.getStudentClasses().add(class2);
    student2.getStudentClasses().add(class3);

    class1.getAssignedStudents().add(student1);
    class1.getAssignedStudents().add(student2);

    class2.getAssignedStudents().add(student1);
    class2.getAssignedStudents().add(student2);

    class3.getAssignedStudents().add(student2);

    session.save(student1);
    session.save(student2);

    
    /*
     Student student = new Student();
     student.setStudentName("Jeremy");
     student.setStudentAge("45");
    
     Project proj1 = new Project("Secure systems", "Spring");
     proj1.setStudent(student);
     Project proj2 = new Project("Databases", "Spring");
     proj2.setStudent(student);
    
     student.getStudentProjects().add(proj1);
     student.getStudentProjects().add(proj2);

     session.save(student);
     session.save(proj1);
     session.save(proj2);
     */

    /*
     Student student = new Student();
     student.setStudentName("JavaFun");
     student.setStudentAge("19");

     StudentInformation studentInfo = new StudentInformation();
     studentInfo.setAddress("1st Street");
     studentInfo.setPhoneNumber("982349823");
     studentInfo.setRegistryNumber("ax203");
     studentInfo.setEnlisted(new Date());

     studentInfo.setStudent(student);
     student.setStudentInfo(studentInfo);

     session.save(student);
     */
    session.getTransaction().commit();
    System.out.println("Great! Student was saved");
  }
}
