package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Student implements Serializable {

  private Integer studentId;
  private String studentName;
  private String studentAge;
  private StudentInformation studentInfo;
  private Set<Project>  studentProjects = new HashSet<Project>(0);
  private Set <Class> studentClasses = new HashSet<Class>(0);


  public Student() {
  }

  
  public Student(String studentName, String studentAge) {
    this.studentName = studentName;
    this.studentAge = studentAge;
  }

  public Integer getStudentId() {
    return studentId;
  }

  public void setStudentId(Integer studentId) {
    this.studentId = studentId;
  }

  public String getStudentName() {
    return studentName;
  }

  public void setStudentName(String studentName) {
    this.studentName = studentName;
  }

  public String getStudentAge() {
    return studentAge;
  }

  public void setStudentAge(String studentAge) {
    this.studentAge = studentAge;
  }

  public StudentInformation getStudentInfo() {
    return studentInfo;
  }

  public void setStudentInfo(StudentInformation studentInfo) {
    this.studentInfo = studentInfo;
  }
  
  public Set<Project> getStudentProjects() {
    return studentProjects;
  }

  public void setStudentProjects(Set<Project> studentProjects) {
    this.studentProjects = studentProjects;
  }

  public Set<Class> getStudentClasses() {
    return studentClasses;
  }

  public void setStudentClasses(Set<Class> studentClasses) {
    this.studentClasses = studentClasses;
  }

}
