create database if not exists `distributedSystems`
  default character set utf8;

use `distributedSystems`

drop table if exists `users`;
create table if not exists `users` (
  `user_id` int(10) unsigned not null auto_increment,
  `username` varchar(10) not null,
  `password`  varchar(10) not null,
  `type` int(3) not null,
  primary key (`user_id`) 
) engine = InnoDB;

drop table if exists `user_details`;
create table if not exists `user_details`(
  `user_id` int(10) not null,
  `name` varchar(15) null,
  `surname` varchar(15) null,
  `latitude` double null,
  `longitude` double null,
  primary key (`user_id`)
) engine = InnoDB;

-- -----------------------------------------------------
-- Data for table `users`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `users` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'admin', 'admin', 0);
INSERT INTO `users` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'user', 'user', 1);

COMMIT;

-- -----------------------------------------------------
-- Data for table `user_details`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `user_details` (`user_id`, `name`, `surname`, `latitude`, `longitude`) VALUES (1, 'admin', 'admin', 40.71417, -74.00639);
INSERT INTO `user_details` (`user_id`, `name`, `surname`, `latitude`, `longitude`) VALUES (2, 'user', 'user', -74.00639, -74.00639);

COMMIT;
