﻿using System;
using System.Collections.Generic;

namespace EntityFramework
{
  public interface DataAccess
  {
    void AddUser(UserDao user);
    void UpdateUser(UserDao user);
    List<UserDao> GetAllUsers();
    UserDao GetUserById(int userId);
    UserDao GetUserByUsername(String username);
    void RemoveUser(int userId);
    void RemoveUser(UserDao user);
  }
}
