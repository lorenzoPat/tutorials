﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WebServiceConsumer
{
  class Program
  {
    static void Main(string[] args)
    {
      MathAddSubService.MathWebService ASPWebService = new MathAddSubService.MathWebService();
      JavWebService.MathWebService JavaWebService = new JavWebService.MathWebService();

      Console.WriteLine("Calling ASP services: ");
      Console.WriteLine("12 + 34 = " + ASPWebService.Add(12, 34));
      Console.WriteLine("12 - 34 = " + ASPWebService.Subtract(12, 34));
      Console.WriteLine("10 * 5 = " + JavaWebService.multiply(10, 5));
      Console.WriteLine("10 / 5 = " + JavaWebService.divide(10, 5));
      
      Console.ReadLine();

    }
  }
}
