package lorenzo.pat.web.service;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

@WebService(serviceName = "MathWebService")
public class MathWebService {
  
  @WebMethod(operationName = "multiply")
  public int multiply(@WebParam(name = "a") int a, @WebParam(name = "b") int b) {
    //TODO write your implementation code here:
    return a * b;
  }
  
  @WebMethod(operationName = "divide")
  public int divide(@WebParam(name = "a") int a, @WebParam(name = "b") int b) {
    //TODO write your implementation code here:
    return a / b;
  }
  
}
