package message.producer;

import common.Entity;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

@Stateless
public class ProducerSB implements ProducerSBLocal {

  //@Resource(name = "mail/testMail")
  //private Session jmstestjmspoolfactory;
  @Resource(mappedName = "jms/testjms")
  private Queue testjms;
  @Inject
  @JMSConnectionFactory("jms/testjmspoolfactory")
  private JMSContext context;

  @Resource(mappedName = "jms/testjmspoolfactory")
  private ConnectionFactory connectionFactory;

  @Resource(mappedName = "jms/testjms")
  private Queue queue;

  @Override
  public void sendMessageToQueue(String msg) {
    sendJMSMessageToTestjms(msg);
  }

  @Override
  public void sendMessageToQueue(String email, String subject, String body) {
    /*
     try {
     sendMail(email, subject, body);
     } catch (NamingException | MessagingException ex) {
     Logger.getLogger(ProducerSB.class.getName()).log(Level.SEVERE, null, ex);
     }
     */
  }

  @Override
  public void sendMessage() {
    // = getSession();
    try {
      Connection connection = connectionFactory.createConnection();
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer messageProducer = session.createProducer(queue);

      MapMessage mapMsg = createMapMessage(session);
      ObjectMessage objMsg = createObjectMessage(session);
      
      messageProducer.send(mapMsg);
      messageProducer.send(objMsg);
      messageProducer.close();
    } catch (JMSException ex) {
      Logger.getLogger(ProducerSB.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private MapMessage createMapMessage(Session session) throws JMSException {
    MapMessage msg = session.createMapMessage();
    msg.setString("subject", "dummy subject");
    msg.setString("body", "dummy body");
    return msg;
  }

  private ObjectMessage createObjectMessage(Session session) throws JMSException {
    Entity ent = new Entity();
    ent.setName("name of entity");
    ent.setStock(23);

    ObjectMessage objMsg = session.createObjectMessage();
    objMsg.setObject(ent);
    
    return objMsg;
  }

  /*
   private void sendMail(String email, String subject, String body) throws NamingException, MessagingException {
   MimeMessage message = new MimeMessage(jmstestjmspoolfactory);
   message.setSubject(subject);
   message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(email, false));
   message.setText(body);
   Transport.send(message);
   }
   */
  private void sendJMSMessageToTestjms(String messageData) {
    context.createProducer().send(testjms, messageData);
  }

  private void sendJMSMessageToTestjms(Message messageData) {
    context.createProducer().send(testjms, messageData);
  }

}
