package message.producer;

import javax.ejb.Local;

@Local
public interface ProducerSBLocal {
  public void sendMessageToQueue(String msg);
  public void sendMessageToQueue(String email, String subject, String body);
  public void sendMessage();
}
