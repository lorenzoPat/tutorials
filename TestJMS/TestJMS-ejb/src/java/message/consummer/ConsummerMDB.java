package message.consummer;

import common.Entity;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@MessageDriven(activationConfig = {
  @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/testjms"),
  @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class ConsummerMDB implements MessageListener {
  
  public ConsummerMDB() {
  }
  
  @Override
  public void onMessage(Message message) {
    if (message instanceof MapMessage) {
      MapMessage msg = (MapMessage)message;
      try {
        System.out.println("I have received mapMessage: " + msg.getString("subject") + " " + msg.getString("body"));
      } catch (JMSException ex) {
        Logger.getLogger(ConsummerMDB.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    
    if (message instanceof ObjectMessage) {
      ObjectMessage msg = (ObjectMessage)message;
      try {
        Entity ent = (Entity)msg.getObject();
        System.out.println("I have received objectMessage: " + ent.getName() + " " + ent.getStock());
      } catch (JMSException ex) {
        Logger.getLogger(ConsummerMDB.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
}
