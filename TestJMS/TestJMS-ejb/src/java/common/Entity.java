package common;

import java.io.Serializable;

public class Entity implements Serializable {
  
  private String name;
  private int stock;

  public Entity() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStock() {
    return stock;
  }

  public void setStock(int stock) {
    this.stock = stock;
  }
  
}
