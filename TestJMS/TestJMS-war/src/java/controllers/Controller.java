/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import message.producer.ProducerSBLocal;


@ManagedBean(name="controller")
@RequestScoped
public class Controller {
  @EJB
  private ProducerSBLocal producerSB;

  private String mail;
  private String body;
  private String subject;

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }
  
  public Controller() {
  }

  public String syncProcess() {
    System.out.println("sync process");
    return null;
  }

  public String asyncProcess() {
    producerSB.sendMessage();
    //producerSB.sendMessageToQueue("dummy message");
    return null;
  }
  
  public String sendMail() {
    //producerSB.sendMessageToQueue(mail, subject, body);
    return null;
  }
  
}
